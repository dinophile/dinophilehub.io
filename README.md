# My First React App!

Do I know what I'm doing yet? Um...kind of! This is a note taking app that allows you to find a Github user, see their profile picture, and some stats about their profile and then add a note abou them that is stored in a Firebase database!

This was a lot of fun to build! I followed along with Egghead.io's React app tutorial run by Tyler McGuinnis. Very well explained and we even refactored to es6. Though there is one component that wouldn't play nicely with the es6 syntax. I'm looking into findout out why. I do know that I can't use multiple JSX tags and comparison operators together. I had to put that one line into a JS if statement in order to get it to render at all! Webpack was not a fan of the JSX comparison and multiple tags! 

I also was not able to refactor that same component using statless functions possibly because of the same problem I'm having with the multiple tags issue? Am trying to find out why!

Great introduction and now on to try to build something simple on my own (after perhaps aother tutorial or two!)
