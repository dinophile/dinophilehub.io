// var React = require('react');
import React from 'react';
// var ReactDOM = require('react-dom');
// var SearchGithub = require('./SearchGithub');
import SearchGithub from './SearchGithub';

// stateless component refactoring
const Main = ({history, children}) => {
  return (
    <div className="main-container">
     <nav className="navbar navbar-default" role="navigation">
        <div className="col-sm-7 col-sm-offset-2" style={{marginTop: 15}}>
          <SearchGithub history = {history}/>
        </div>
      </nav>
      <div className="container">
        {children}
      </div>
    </div>
  )
}

export default Main;


// es6 refactoring:
// class Main extends React.Component {
//   render(){
//     return (
//       <div className="main-container">
//        <nav className="navbar navbar-default" role="navigation">
//           <div className="col-sm-7 col-sm-offset-2" style={{marginTop: 15}}>
//             <SearchGithub history = {this.props.history}/>
//           </div>
//         </nav>
//         <div className="container">
//           {this.props.children}
//         </div>
//       </div>
//     )
//   }
// }


// var Main = React.createClass({
//   render: function(){
//     return (
//       <div className="main-container">
//        <nav className="navbar navbar-default" role="navigation">
//           <div className="col-sm-7 col-sm-offset-2" style={{marginTop: 15}}>
//             <SearchGithub />
//           </div>
//         </nav>
//         <div className="container">
//           {this.props.children}
//         </div>
//       </div>
//     )
//   }
// });

// React.render(<Main />, document.getElementById('app'));
// module.exports = Main;
