// var React = require('react');
import React from 'react';

// es6 refactored to use statless components where available
const Home = () => {
  return (
    <h2 className="text-center">
      Search by Github Username Above
    </h2>
  )
}

export default Home;

// original creatClass syntax refactored to es6
// class Home extends React.Component {
//   render(){
//     return (
//       <h2 className="text-center">
//         Search by Github Username Above
//       </h2>
//     )
//   }
// }
//


// var Home = React.createClass({
//   render: function(){
//     return (
//       <h2 className="text-center">
//         Search by Github Username Above
//       </h2>
//     )
//   }
// })
//
// module.exports = Home;
