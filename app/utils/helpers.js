// var axios = require('axios');
import axios from 'axios'

function getRepos(username){
  // return axios.get('https://api.github.com/users/' + username + '/repos');
  return axios.get(`https://api.github.com/users/${username}/repos`);
};

function getUserInfo(username){
  // return axios.get('https://api.github.com/users/' + username);
  return axios.get(`https://api.github.com/users/${username}`);
};

// const helpers = {
// so now we can use import getGithubInfo from '.././helpers' wherever we have
// used require syntax for the getGithubInfo function (profile.js)
export default function  getGithubInfo(username){
  return axios.all([getRepos(username), getUserInfo(username)])
  .then((arr) =>({repos: arr[0].data, bio: arr[1].data}));
}
// };
//
// module.exports = helpers;
// export default helpers;
